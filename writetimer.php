<html>
<body>

<?php

$submitTime = $_POST["timeval"];
$choice = $_POST["time"];

//var d = new Date("2015-03-25T12:00:00Z")
$nowDate = date("Y-m-d") . "T";
$nowTime = date("H:i:s");

if ($choice == "counttotime" or $choice == "countfromtime") {

	$targetTime = $nowDate . $submitTime . "Z";
	if ($choice == "counttotime") {
		$mode = "countdown";
	} else {
		$mode = "countup";
	}
} else if ($choice == "countdown") {
	$total = (date("H") + (int)substr($submitTime, 0, 2))*60*60 + (date("i") + (int)substr($submitTime, 3, 2))*60 + (date("s") + (int)substr($submitTime, 6, 2));

	$h = (int)floor($total / (60 * 60));
	$m = (int)floor(($total / 60) % 60);
	$s = (int)floor($total % 60);

	$h = str_pad($h, 2, '0', STR_PAD_LEFT);
	$m = str_pad($m, 2, '0', STR_PAD_LEFT);
	$s = str_pad($s, 2, '0', STR_PAD_LEFT);

	$targetTime = $nowDate . $h . ":" . $m . ":" . $s . "Z";
	
	$mode = "countdown";
} else if ($choice == "countfromnow") {
	$targetTime = $nowDate . $nowTime . "Z";
	$mode = "countup";
} else {
	$targetTime = "";
	$mode = "clock";
}

$myfile = fopen("targettime.txt", "w") or die("Unable to open file");
fwrite($myfile, $targetTime);
fclose($myfile);

$modefile = fopen("mode.txt", "w") or die("Error opening mode.txt");
fwrite($modefile, $mode);
fclose($modefile);

header("Location: control.html");
?>

</body>
</html>
