function loadFile(filePath) {
	return new Promise(function(resolve, reject) {
		var request = new XMLHttpRequest();
		request.onreadystatechange = function() {
			if (this.readyState == 4) {
				if (this.status == 200) {
					resolve(this.responseText);
				} else {
					reject(new Error("Error fetching URL"));
				}
			}
		}
		request.open("GET", filePath + "?q=" + Math.random(), true);
		request.send();
	});
}

function pad(num) {
	var str = "00" + num;
	return str.substr(-2);
}

// Only sets the next timeout after the previous request has completed.
function infiniteFetch(f) {
	var func;
	func = function() {
		return f().then(function() {
			setTimeout(func, 1000);
		}).catch(function(e) {
			console.error(e);
			setTimeout(func, 1000);
		});
	};
	func();
}

function infiniteFetchFile(name, f) {
	infiniteFetch(function() {
		return loadFile(name).then(f);
	});
}

infiniteFetchFile("message.txt", function(msgtext) {
	document.getElementById("msg").innerText = msgtext;
});

infiniteFetchFile("currTitle.txt", function(titletext) {
	document.getElementById("currentTitle").innerText = titletext;
});

var mode;
var tttext;


// Read from target time file
infiniteFetchFile("targettime.txt", function(res) {
	tttext = res;
});

// Get current mode
infiniteFetchFile("mode.txt", function(res) {
	mode = res;
});

// Update clock
setInterval(function() {
	if (!mode) {
		return;
	}
	var hours;
	var minutes;
	var seconds;
	var distance;
	if (mode === "clock") {
		var now = new Date();
		hours = now.getHours();
		minutes = now.getMinutes();
		seconds = now.getSeconds();
	} else {
		// Set the date we're counting down to
		var countDate = new Date(tttext).getTime();

		// Get today's date, time and timezone offset
		var d = new Date();
		var now = d.getTime();
		var n = d.getTimezoneOffset();
		var tz = n * 60000;
		now = now - tz;

		if (mode === "countup") {
			distance = now - countDate;
		} else {
			// Find the distance between now and the count down date
			distance = countDate - now;
		}
		// Time calculations for days, hours, minutes and seconds
		hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
		minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
		seconds = Math.floor((distance % (1000 * 60)) / 1000);
	}

	// Display the result in the element with id="demo"
	document.getElementById("timer").innerText = pad(hours) + ":" + pad(minutes) + ":" + pad(seconds);

	if (mode === "countdown") {
		// If the count down is finished, write some text
		if (distance < 0) {
			//clearInterval(x);
			document.getElementById("timer").innerText = "END";
		}

		//set some css
		if (distance < 0) {
			//less than 0
			document.getElementById("countdown").style.cssText = `
			background-color: black;
			animation-name: oneinone;
			animation-duration: 1s;
			animation-iteration-count: infinite;
			`;
		} else if (distance < (1 * 60 * 1000)) {
			//less than 1 min
			document.getElementById("countdown").style.cssText = `
			background-color: orange;
			animation-name: oneinfive;
			animation-duration: 5s;
			animation-iteration-count: infinite;
			`;
		} else if (distance < (5 * 60 * 1000)) {
			//less than 5 min
			document.getElementById("countdown").style.cssText = `
			background-color: orange;
			animation: none; /*sets all animate-* properties to initial value*/
			`;
		} else if (distance > (5 * 60 * 1000)) {
			document.getElementById("countdown").style.cssText = `
			background-color: black;
			animation: none; /*sets all animate-* properties to initial value*/
			`;
		}
	} else {
		document.getElementById("countdown").style.cssText = `
		background-color: black;
		animation: none; /*sets all animate-* properties to initial value*/
		`;
	}
}, 200);

document.querySelector(":root").addEventListener("click", function() {
	document.querySelector(":root").requestFullscreen();
});
