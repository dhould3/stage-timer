async function loadFile(filePath) {
	let c = filePath.includes("?") ? "&" : "?";
	let res = await fetch(filePath + c + "q=" + Math.random());
	if (res.status !== 200) {
		throw new Error("Status code is not 200");
	}
	return await res.text();
}

function pad(num) {
	var str = "00" + num;
	return str.substr(-2);
}

function fmtTime(time) {
	return fmtTimeHMS(time.getHours(), time.getMinutes(), time.getSeconds());
}

function fmtTimeHMS(hours, mins, secs) {
	return pad(hours) + ":" + pad(mins) + ":" + pad(secs);
}

// Only sets the next timeout after the previous request has completed.
function infiniteFetch(f) {
	let func;
	func = async () => {
		try {
			await f();
		} catch (e) {
			console.error(e);
		}
		setTimeout(func, 1000);
	};
	func();
}

async function loadCsvFromServer() {
	let csv = await loadFile("file.csv");
	document.querySelector("#csv-input").textContent = csv;
	let table = document.querySelector("#table");
	while (table.childNodes.length > 1) {
		table.lastChild.remove();
	}
	let i = 0;
	for (let entry of csv.split("\n")) {
		if (entry) {
			let row = document.createElement("tr");
			for (let item of entry.split(",")) {
				let td = document.createElement("td");
				td.innerText = item;
				row.insertBefore(td, null);
			}
			row.tabIndex = 0;
			let thisI = i;
			row.addEventListener("click", () => {
				setRow(thisI);
			});
			row.addEventListener("keydown", e => {
				if (e.key === "Enter") {
					setRow(thisI);
				}
			});
			table.insertBefore(row, null);
			i += 1;
		}
	}
}

async function updateCurrentRow() {
	var item = Number(await loadFile("currItem.txt"));

	var els = document.getElementById("table").getElementsByTagName("tr");
	for (let row of els) {
		row.classList.remove("currentRow");
	}
	if (item < els.length - 1) {
		els[item + 1].classList.add("currentRow");
	}
}

async function reloadCsv() {
	await loadCsvFromServer();
	await updateCurrentRow();
}

reloadCsv();

async function setTitle(title) {
	let form = new FormData();
	form.append("comment", title);
	await fetch("writeCurrTitle.php", {
		"method": "POST",
		"body": form
	});
}

async function setTime(type, value) {
	let time = new FormData();
	time.append("time", type);
	time.append("timeval", value);
	await fetch("writetimer.php", {
		"method": "POST",
		"body": time
	});
}

async function setMessage(message) {
	let form = new FormData();
	form.append("comment", message);
	await fetch("writemsg.php", {
		"method": "POST",
		"body": form
	});
}

async function setCsv(csv) {
	let form = new FormData();
	form.append("comment", csv);
	await fetch("writeCsvFile.php", {
		"method": "POST",
		"body": form
	});
	await reloadCsv();
}

document.querySelector("#prev-button").addEventListener("click", () => go(-1));
document.querySelector("#curr-button").addEventListener("click", () => go(0));
document.querySelector("#next-button").addEventListener("click", () => go(1));

async function go(move){
	let currItem = await loadFile("currItem.txt");
	setRow(parseInt(currItem) + move);
}

async function setRow(itemToGo) {
	if (itemToGo < 0) {
		return;
	}
	let str = await loadFile("getrow.php?row=" + itemToGo);
	if (!str.length) {
		return;
	}
	await fetch("writeCurrItem.php?val=" + itemToGo);
	let rowData = str.split(",");
	await setTitle(rowData[0].trim());
	await setTime("countdown", rowData[1].trim());
	await updateCurrentRow();
}

infiniteFetch(async () => {
	// message
	let msgtext = await loadFile("message.txt");
	document.getElementById("msgdisp").innerText = msgtext;
});

infiniteFetch(async () => {
	// title
	let titletext = await loadFile("currTitle.txt");
	document.getElementById("titledisp").innerText = titletext;
});

document.querySelector("#title-submit").addEventListener("click", async e => {
	await setTitle(document.querySelector("#title-input").value);
});

document.querySelector("#message-submit").addEventListener("click", async e => {
	await setMessage(document.querySelector("#message-input").value);
});

document.querySelector("#timer-submit").addEventListener("click", async e => {
	let mode;
	for (let radio of document.querySelectorAll('input[type="radio"][name="time"]')) {
		if (radio.checked) {
			mode = radio.value;
			break;
		}
	}
	await setTime(mode, document.querySelector("#timer-time").value);
});

document.querySelector("#csv-submit").addEventListener("click", async e => {
	await setCsv(document.querySelector("#csv-input").value);
});

let oldTimeInput;

function updateTimeInput() {
	let timeInput = document.querySelector("#timer-time");
	if (document.querySelector("#timer-from-now").checked || document.querySelector("#timer-clock").checked) {
		if (!timeInput.disabled) {
			oldTimeInput = timeInput.value;
		}
		timeInput.disabled = true;
		timeInput.value = fmtTime(new Date());
	} else {
		if (timeInput.disabled) {
			timeInput.value = oldTimeInput;
		}
		timeInput.disabled = false;
	}
}

for (let radio of document.querySelectorAll('input[type="radio"][name="time"]')) {
	radio.addEventListener("change", updateTimeInput);
}

let tttext;
let mode;

infiniteFetch(async () => {
	// Read from target time file
	tttext = await loadFile("targettime.txt");
});

infiniteFetch(async () => {
	// Get current mode
	mode = await loadFile("mode.txt");
});

function updateClock() {
	// Get today's date and get h m s from that
	// Time zone may need attention.
	document.getElementById("clockdisp").innerText = fmtTime(new Date());
	updateTimeInput();
}

updateClock();

setInterval(async function() {
	updateClock();

	if (!mode) {
		return;
	}

	let hours;
	let minutes;
	let seconds;
	let distance;
	if (mode === "clock") {
		let now = new Date();
		hours = now.getHours();
		minutes = now.getMinutes();
		seconds = now.getSeconds();
	} else {
		// Set the date we're counting down to
		let countDate = new Date(tttext).getTime();

		// Get today's date, time and timezone offset
		let d = new Date();
		let now = d.getTime();
		let n = d.getTimezoneOffset();
		let tz = n * 60000;
		now = now - tz;

		if (mode === "countup") {
			distance = now - countDate;
		} else {
			// Find the distance between now and the count down date
			distance = countDate - now;
		}
		// Time calculations for days, hours, minutes and seconds
		hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
		minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
		seconds = Math.floor((distance % (1000 * 60)) / 1000);
	}

	// Display the result in the element with id="demo"
	document.getElementById("timerdisp").innerText = fmtTimeHMS(hours, minutes, seconds);

	// If the count down is finished, write some text
	if (distance < 0 && mode === "countdown") {
		document.getElementById("timerdisp").innerText = "END";
	}

}, 200);
